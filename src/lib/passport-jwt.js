import { Strategy, ExtractJwt } from 'passport-jwt';
import config from 'config';

import { User } from '../db/postgres/models';

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.get('jwt.secret')
};

export default (passport) => {
  passport.use(new Strategy(opts, async (payload, done) => {
    const user = await User.getById(payload.id);
    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  }))
};

