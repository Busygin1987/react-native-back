import strategy from 'passport-facebook';
import config from 'config';

import { User } from '../db/postgres/models';

const FacebookStrategy = strategy.Strategy;


export default (passport) => {
  passport.serializeUser((user, done) => {
    return done(null, user.id);
  });

  passport.deserializeUser(async (id, done) => {
    try {
      const user = await User.findOne({ id })
      done(null, user);
    } catch (err) {
      done(err);
    }
  });

  passport.use(
    new FacebookStrategy(
      {
        clientID: config.get('facebook.appID'),
        clientSecret: config.get('facebook.appSecret'),
        callbackURL: config.get('facebook.callbackUrl'),
        profileFields: ['id', 'displayName', 'photos', 'email', 'name', 'birthday'],
        enableProof: true
      },
      async (accessToken, refreshToken, profile, done) => {
        try {
          const { email, first_name, last_name, id, picture } = profile._json;
          // console.log('profile picture', profile)
          const user = await User.findOrCreate({
            where: { facebook_id: id },
            defaults: {
            email: email,
            firstName: first_name,
            lastName: last_name,
            facebookId: id,
            avatar: picture && picture.data.url
          }});
          done(null, user);
        } catch (err) {
          done(err, null);
        }
      }
    )
  );
}