import Router from 'koa-router';
import * as ctrl from '../controllers/auth';
import passport from 'passport';
// import validate from '../middleware/validate';
// import schemas from './schemas/auth';

const router = new Router();

router
  // .get('/facebook', async(ctx, next) => {
  //   await passport.authenticate('facebook', { scope: [ 'email', 'public_profile' ], session: false })(ctx, next);
  // })
  // .get('/facebook/callback', async(ctx, next) => {
  //     passport.authenticate('facebook',
  //     {
  //       failureRedirect: '/',
  //       session: false
  //     },
  //     () => {
  //       console.log(1111, ctx.state)
        // const user = req.user;
        // const payload = {
        //     id: user._id,
        //     email: user.email,
        // };
      
        // jwt.sign(payload, secret, {expiresIn: 3600}, (err, token) => {
        //     if (err) {
        //         res.status(500).send({
        //             error: 'Error signing token',
        //             raw: err,
        //         });
        //     }
        //     // Отдаем html
        //     res.send(`
        //     <script>
        //       localStorage.setItem('token', '${`Bearer ${token}`}');
        //       localtion.href = '/'
        //     </script>
        //     `);
        }
  //     )(ctx, next);
  //   }
  // )
  .post('/sign-in', ctrl.login)
  .post('/sign-up', ctrl.signUp)
  .get('/logout', ctrl.logout);

export default router.routes();