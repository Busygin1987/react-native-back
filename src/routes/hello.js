import Router from 'koa-router';

const router = new Router();

router
  .get('/', (ctx) => ctx.body = 'HELLO WORLD');

export default router.routes();