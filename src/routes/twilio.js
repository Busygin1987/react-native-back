import Router from 'koa-router';
import * as ctrl from '../controllers/twilio';
import passport from 'koa-passport';

const router = new Router();

router.get('/token', passport.authenticate('jwt', { session: false }), ctrl.getTwilioToken);
router.get('/rooms', passport.authenticate('jwt', { session: false }), ctrl.getListTwilioRooms);
router.post('/room', passport.authenticate('jwt', { session: false }), ctrl.createTwilioRoom);
router.put('/room/:roomSid', passport.authenticate('jwt', { session: false }), ctrl.updateTwilioRoom);

export default router.routes();