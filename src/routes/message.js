import Router from 'koa-router';
import * as ctrl from '../controllers/message';
import passport from 'koa-passport';

// import validate from '../middleware/validate';
// import schemas from './schemas/user';

const router = new Router();

router
  .get('/:recipient_id', passport.authenticate('jwt', { session: false }), ctrl.getMessages)
  .get('/:id', passport.authenticate('jwt', { session: false }), ctrl.getMessage)
  .post('/', passport.authenticate('jwt', { session: false }), ctrl.createMessage)
  .put('/:id', passport.authenticate('jwt', { session: false }), ctrl.updateMessage)
  .delete('/:id', passport.authenticate('jwt', { session: false }), ctrl.deleteMessage);

export default router.routes();