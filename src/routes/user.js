import Router from 'koa-router';
import passport from 'koa-passport';

import * as ctrl from '../controllers/user';

// import validate from '../middleware/validate';
// import schemas from './schemas/user';

const router = new Router();

router
  .get('/', passport.authenticate('jwt', { session: false }), ctrl.getUser)
  .post('/', ctrl.createUser)
  .get('/friends', passport.authenticate('jwt', { session: false }), ctrl.getUserFriends)
  .get('/all', passport.authenticate('jwt', { session: false }), ctrl.getAllUsers)
  .delete('/friend/:id', passport.authenticate('jwt', { session: false }), ctrl.deleteFriend)
  .post('/friend', passport.authenticate('jwt', { session: false }), ctrl.addNewFriend);

export default router.routes();