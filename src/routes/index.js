import Router from 'koa-router';
import compose from 'koa-compose';

import auth from './auth';
import user from './user';
import message from './message';
import ws from './ws';
import hello from './hello';
import twilio from './twilio';

const router = new Router();

router.use('/auth', auth);
router.use('/user', user);
router.use('/message', message);
router.use('/ws', ws);
router.use('/hello', hello);
router.use('/twilio', twilio);

export default compose([router.routes(), router.allowedMethods()]);