import User from './user';
import Message from './message';
import Friend from './friend';

User.hasMany(Message, { as: 'messages', foreignKey: 'id', onDelete: 'CASCADE' });
Message.belongsTo(User, { as: 'author', foreignKey: 'from' });
Message.belongsTo(User, { as: 'recipient', foreignKey: 'to' });
User.hasMany(Friend, { as: 'friends', foreignKey: 'user_id', onDelete: 'CASCADE' });
User.hasMany(Friend, { as: 'chums', foreignKey: 'friend_id', onDelete: 'CASCADE' });
Friend.belongsTo(User, { as: 'friends', foreignKey: 'id' });

export {
  User,
  Message,
  Friend
};