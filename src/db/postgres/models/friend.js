import _ from 'lodash';
import Sequelize, { Op } from 'sequelize';
import sequelize from '../postgres';

const Model = Sequelize.Model;

export default class Friend extends Model {
  static deleteById(id, userId) {
    return this.destroy({
      where: {
        [Op.and]: [
          { friend_id: id },
          { user_id: userId }
        ]
      },
    });
  }

  static checkIsFriend(userId, friendId) {
    return this.findOne({
      where: {
        [Op.and]: [
          { friend_id: userId },
          { user_id: friendId }
        ]
      }
    })
  }

  publish() {
    return _.pick(this, user);
  }
}

Friend.init({
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false,
    primaryKey: true
  },
  user_id: {
    type: Sequelize.UUID,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  friend_id: {
    type: Sequelize.UUID,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  createdAt: {
    allowNull: true,
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: Sequelize.DATE
  }
}, {
  timestamps: true,
  paranoid: true,
  sequelize,
  modelName: 'friend',
  tableName: 'Friends',
  freezeTableName: true
});
