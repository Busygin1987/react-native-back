import passport from 'koa-passport';
import passportJwtConfig from '../lib/passport-jwt';

passportJwtConfig(passport);

export default passport.initialize();