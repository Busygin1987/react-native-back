import passport from 'koa-passport';
import passportFBConfig from '../lib/passport-facebook';

passportFBConfig(passport);

export default passport.initialize();