import io from 'socket.io';
import { Message } from './db/postgres/models';

export default (server) => {
  const socketIO = io(server);

  socketIO.on('connection', (socket) => {
    socket.on('event://send-message', async (msg) => {
      console.log("got", msg);
  
      try {
        const data = JSON.parse(msg);
  
        await Message.create(data);
  
        socket.broadcast.emit('event://get-message', JSON.stringify({
          userId: data.from,
          message: data.body,
          room_id: data.room_id
        }));
      } catch (error) {
        console.error(error)
      }
    })
  });
};
