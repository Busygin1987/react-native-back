import Koa from 'koa';
import body from 'koa-bodyparser';
import convert from 'koa-convert';
import logger from 'koa-logger';
import json from 'koa-json';
import cors from 'koa-cors';
import https from 'https';
import fs from 'fs';
import config from 'config';

import errorHandler from './middleware/error-handler';
import passportJwt from './middleware/passport-jwt-init';
import passportFB from './middleware/passport-fb-init';
import serverSocket from './ws';

import routes from './routes';

const app = new Koa();

const PORT = config.get('server.port');

const server = https.createServer({
  key: fs.readFileSync(process.cwd() + '/src/key.pem'),
  cert: fs.readFileSync(process.cwd() + '/src/cert.pem'),
  passphrase: 'YOUR PASSPHRASE HERE'
}, app.callback());

serverSocket(server, { pingTimeout: 30000 });
server.listen(PORT);

app.use(passportFB);
app.use(passportJwt);
app.use(errorHandler());
app.use(convert(cors()));
app.use(logger());
app.use(json());
app.use(body({ formLimit: '100mb', jsonLimit: '16mb' }));
app.use(routes);

export default server;
